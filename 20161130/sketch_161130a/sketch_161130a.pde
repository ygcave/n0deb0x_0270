int cols = 10;
int rows;
float maxW = 0.0;
//num of rectangle : mouseY
int num = 20;
//color mode(true or false) : fill or stroke
boolean colorMode = true;

float maxAngle = 0.0;
float maxAngleSpeed = 2.0;
//rotate mode(1 or 2 or 3) : right or left or right/left
int rotateMode = 1;
//animation mode(true or false) : auto or mouseX
boolean aniMode = true;

void setup() {
  size(800, 800);
  background(0);
  rows = cols;
  maxW = width/cols;
}

void draw() {
  background(0);

  num = int(map(mouseY, 0, height, 2, 20));

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      for (int i=0; i<num; i++) {
        pushMatrix();
        translate(maxW/2+maxW*x, maxW/2+maxW*y);

        //float angle = map(i, 0, num-1, 0, map(mouseX, 0, width, 0, 360));
        float angle = map(i, 0, num-1, 0, maxAngle);
        /*
        if((x%2==0 && y%2==0) || (x%2==1 && y%2==1)){
         angle = angle * 1;
         }else{
         angle = angle * (-1);
         }*/

        if (rotateMode == 1) {
          angle = angle * 1;
        } else if (rotateMode == 2) {
          angle = angle * (-1);
        } else if (rotateMode == 3) {
          if ((x+y)%2==0) {
            angle = angle * 1;
          } else {
            angle = angle * (-1);
          }
        }

        rotate(radians(angle));
        float w = map(i, 0, num-1, maxW, 10);

        if (colorMode == true) {
          noFill();
          stroke(255);
        } else {
          float c = map(i, 0, num-1, 0, 255);
          noStroke();
          fill(0, c, c);
        }

        rect(-w/2, -w/2, w, w);
        popMatrix();
      }
    }
  }

  if (aniMode == true) {
    maxAngle = maxAngle + maxAngleSpeed;
  }else{
    maxAngle = map(mouseX, 0, width, 0, 360);
  }
  if (maxAngle > 360) {
    maxAngle = 360;
    maxAngleSpeed = maxAngleSpeed * (-1);
  }
  if (maxAngle < 0) {
    maxAngle = 0;
    maxAngleSpeed = maxAngleSpeed * (-1);
  }
}

void keyPressed() {
  if (key == 'c') {
    colorMode = !colorMode;
  }
  if (key == ' ') {
    saveFrame("myWork_####.jpg");
  }
  if (key == 'r') {
    rotateMode = rotateMode + 1;
    if (rotateMode > 3) {
      rotateMode = 1;
    }
    //println("rotate mode : " + rotateMode);
  }
  if(key == 'a'){
    aniMode = !aniMode;
  }
}