int counter = 0;
float x = 0.0;
float y = 0.0;
float w = 0.0;
float t = 0.0;

int num = 12;

void setup() {
  size(600, 600);
  background(255, 255, 0);
  println("hello world");
}

void draw() {
  background(255, 255, 0);
  println("hello world : " + counter);

  //fill(255, 0, 255);
  noFill();
  stroke(0, abs(sin(t)*255), 255);
  //rect(x-50, y-50, w, w);
  ellipse(x, y, w, w);

  x = x + 1;
  if (x > 600) {
    x = 0;
  }
  y = height/2 + sin(t)*200;
  w = 50 + cos(t)*50;

  t = t + 0.032;


  if (counter > 600) {
    counter = 0;
  } else {
    counter = counter + 1;
  }

  num = int(map(mouseX, 0, width, 2, 50));

  float cellSize = width/num;
  for (int j=0; j<num; j=j+1) {
    for (int i=0; i<num; i=i+1) {
      if (i%2==0) {
        fill(map(i, 0, num-1, 0, 255), map(j, 0, num-1, 0, 255), 255);
        noStroke();
      } else {
        noFill();
        stroke(map(i, 0, num-1, 0, 255), map(j, 0, num-1, 0, 255), 255);
      }
      float _x = i*cellSize+cellSize/2.0;
      float _y = j*cellSize+cellSize/2.0;
      ellipse(_x, _y, cellSize*0.8, cellSize*0.8);

      //noFill();
      //stroke(0);
      //rect(i*cellSize, j*cellSize, cellSize-2, cellSize-2);
      //println(i);
    }
  }
}