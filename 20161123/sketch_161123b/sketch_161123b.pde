float angle = 0.0;
float angleSpeed = 10.0;
float angleMax = 0.0;
int sizeMax = 10;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  
  noFill();
  stroke(255);
  strokeWeight(1);
  //rotate(radians(angle));
  //rect(0, 0, 600, 600);
  noStroke();
  for(int i=10; i>=1; i=i-1){
    fill(map(i, 1, 10, 255, 0), map(i, 1, 10, 255, 0), 0);
    pushMatrix();
    translate(300, 300);
    rotate(radians(map(i, 1, 10, angleMax, 0)));
    rect(-(i*sizeMax)/2, -(i*sizeMax)/2, i*sizeMax, i*sizeMax);
    popMatrix();
  }
  //rect(-100, -100, 200, 200);
  //rect(-50, -50, 100, 100);
  //rect(-25, -25, 50, 50);
  //rect(-12.5, -12.5, 25, 25);
  
  angle = angle + angleSpeed;
  if(angle > 600){
    angle = 600;
    angleSpeed = angleSpeed*(-1);
  }
  if(angle < 0){
    angle = 0;
    angleSpeed = angleSpeed*(-1);
  }
  
  angleMax = map(angle, 0, 600, 0, 180);
  
  sizeMax = int(map(mouseX, 0, 600, 5, 30));
  
  //noFill();
  fill(255);
  ellipse(300, angleMax, sizeMax, sizeMax);
  ellipse(angleMax, 300, sizeMax, sizeMax);
  
  ellipse(300, map(angleMax, 0, 180, 600, 600-180), sizeMax, sizeMax);
  ellipse(map(angleMax, 0, 180, 600, 600-180), 300, sizeMax, sizeMax);
  
  ellipse(angleMax, angleMax, sizeMax, sizeMax);
  ellipse(map(angleMax, 0, 180, 600, 600-180), map(angleMax, 0, 180, 600, 600-180), sizeMax, sizeMax);

}