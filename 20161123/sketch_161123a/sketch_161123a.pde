int y = 50;
int speedY = 10;

int x = 0;
int speedX = 2;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  
  y = y + speedY;
  
  x = x + speedX;
  
  if(x > 600){
    x = 600;
    speedX = speedX*(-1);
  }
  if(x < 0){
    x = 0;
    speedX = speedX*(-1);
  }
  
  if(y > 550){
    y = 550;
    speedY = speedY*(-1);
  }  
  if(y < 50){
    y = 50;
    speedY = speedY*(-1);
  }
  
  fill(map(x, 0, 600, 0, 255));
  ellipse(x, y, 50, 50);
}